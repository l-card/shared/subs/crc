/*================================================================================================*
 * Контрольные суммы CRC-16 и CRC-32 с повышенной скоростью вычислений
 *================================================================================================*/

#ifndef FAST_CRC_H_
#define FAST_CRC_H_

#include "fast_crc_cfg.h"

#ifdef __cplusplus
extern "C" {
#endif

/*================================================================================================*/
/* Вычисление CRC16 (CCITT, 0x1021) */
#if FASTCRC_CRC16_ADD8
/*------------------------------------------------------------------------------------------------*/
FASTCRC_U16_TYPE CRC16_Add8         /* Добавление байта */
    (
    FASTCRC_U16_TYPE crc,           /* (вх)  - начальное значение */
    FASTCRC_U8_TYPE byte            /* (вх)  - данные */
    );
/*------------------------------------------------------------------------------------------------*/
#endif

#if FASTCRC_CRC16_ADD16
/*------------------------------------------------------------------------------------------------*/
FASTCRC_U16_TYPE CRC16_Add16        /* Добавление 16-битного слова (младший байт первым) */
    (
    FASTCRC_U16_TYPE crc,           /* (вх)  - начальное значение */
    FASTCRC_U16_TYPE word           /* (вх)  - данные */
    );
/*------------------------------------------------------------------------------------------------*/
#endif

#if FASTCRC_CRC16_BLOCK8
/*------------------------------------------------------------------------------------------------*/
FASTCRC_U16_TYPE CRC16_Block8       /* Вычисление CRC16 блока байтов */
    (
    FASTCRC_U16_TYPE crc,           /* (вх)  - начальное значение */
    const FASTCRC_U8_TYPE* pdata,   /* (вх)  - данные */
    FASTCRC_SIZE_TYPE num_bytes     /* (вх)  - размер данных */
    );
/*------------------------------------------------------------------------------------------------*/
#endif

#if FASTCRC_CRC16_BLOCK16
/*------------------------------------------------------------------------------------------------*/
FASTCRC_U16_TYPE CRC16_Block16      /* Вычисление CRC16 блока 16-битных слов */
    (
    FASTCRC_U16_TYPE crc,           /* (вх)  - начальное значение */
    const FASTCRC_U16_TYPE* pdata,  /* (вх)  - данные */
    FASTCRC_SIZE_TYPE num_words     /* (вх)  - размер данных */
    );
/*------------------------------------------------------------------------------------------------*/
#endif

/* Вычисление CRC32 (CCITT) */
#if FASTCRC_CRC32_BLOCK8
/*------------------------------------------------------------------------------------------------*/
FASTCRC_U32_TYPE CRC32_Block8       /* Вычисление CRC32 блока байтов */
    (
    FASTCRC_U32_TYPE crc,           /* (вх)  - начальное значение (при первом блоке 0) */
    const FASTCRC_U8_TYPE* pdata,   /* (вх)  - данные */
    FASTCRC_SIZE_TYPE num_bytes     /* (вх)  - размер данных */
    );
/*------------------------------------------------------------------------------------------------*/
#endif
/*================================================================================================*/

#ifdef __cplusplus
}
#endif

#endif /* FAST_CRC_H_ */
